package com.daom.dao;

import com.daom.entity.Message;
import com.daom.entity.User;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by xiaodao
 * date: 2019/8/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageDaoImplTest {

	@Autowired
	private MessageDao messageDaoImpl;

	@Test
	public void findListByFromAndTo() {
		List<Message> listByFromAndTo = messageDaoImpl.findListByFromAndTo(1001L, 1002L, 1, 10);
		listByFromAndTo.stream().forEach(
			o->{
				System.out.println(o.getFrom().getUsername()+ "--- "+o.getMsg());
			}
		);
	}

	@Test
	public void findMessageById() {
	}

	@Test
	public void updateMessageState() {
	}

	@Test
	public void saveMessage() {
		Message message = Message.builder()
			.id(ObjectId.get()) .msg("你好") .sendDate(new Date()) .status(1)
			.from(new User(1001L, "zhangsan"))
			.to(new User(1002L,"lisi"))
			.build();
		messageDaoImpl.saveMessage(message);
		message = Message.builder()
			.id(ObjectId.get())
			.msg("你也好")
			.sendDate(new Date())
			.status(1)
			.to(new User(1001L, "zhangsan")) .from(new User(1002L,"lisi")) .build();
		messageDaoImpl.saveMessage(message);
		message = Message.builder()
			.id(ObjectId.get())
			.msg("我在学习开发IM")
			.sendDate(new Date())
			.status(1)
			.from(new User(1001L, "zhangsan")) .to(new User(1002L,"lisi")) .build();
		messageDaoImpl.saveMessage(message);
		message = Message.builder()
			.id(ObjectId.get())
			.msg("那很好啊!")
			.sendDate(new Date())
			.status(1)
			.to(new User(1001L, "zhangsan")) .from(new User(1002L,"lisi")) .build();
		messageDaoImpl.saveMessage(message);
		System.out.println("-----");
	}

	@Test
	public void deleteMessage() {
	}
}
