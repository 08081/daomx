package com.daom.controller;

import com.daom.entity.Message;
import com.daom.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/19
 */
@RestController
@RequestMapping("/message")
public class MessageController {

	@Autowired
	private MessageService messageService;

	/**
	 * 拉取消息列表
	 *
	 * @param fromId * @param toId
	 * @param page
	 * @param rows
	 * @return
	 */
	@GetMapping
	public List<Message> queryMessageList(@RequestParam("fromId") Long fromId,
										  @RequestParam("toId") Long toId,
										  @RequestParam(value = "page",
											  defaultValue = "1") Integer page,
										  @RequestParam(value = "rows",

											  defaultValue = "10") Integer rows) {
		return messageService.queryMessageList(fromId, toId, page, rows);
	}
}
