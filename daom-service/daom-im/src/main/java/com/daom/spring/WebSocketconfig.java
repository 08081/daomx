package com.daom.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Created by xiaodao
 * date: 2019/8/17
 */
//@Configuration
//@EnableWebSocket
public class WebSocketconfig implements WebSocketConfigurer {
	@Autowired
	private MyHandshakeInterceptor myHandshakeInterceptor;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(myHandler(), "/ws")

			.setAllowedOrigins("*")
		.addInterceptors(myHandshakeInterceptor);
	}
	@Bean
	public WebSocketHandler myHandler() {
		return new MyHandler();
	}
}
