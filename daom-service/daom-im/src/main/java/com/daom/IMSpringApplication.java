package com.daom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by xiaodao
 * date: 2019/8/17
 */
@SpringBootApplication
public class IMSpringApplication {
	public static void main(String[] args) {
		SpringApplication.run(IMSpringApplication.class,args);
	}
}
