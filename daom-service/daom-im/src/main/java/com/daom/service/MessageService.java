package com.daom.service;

import com.daom.dao.MessageDao;
import com.daom.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/19
 */
@Service
public class MessageService {

	@Autowired
	private MessageDao messageDao;

	public List<Message> queryMessageList(Long fromId, Long toId, Integer page,
										  Integer rows) {
		List<Message> list = this.messageDao.findListByFromAndTo(fromId, toId,
			page, rows);
		for (Message message : list) {
			if (message.getStatus().intValue() == 1) {
				this.messageDao.updateMessageState(message.getId(), 2);
			}

		}
		return list;

	}

}
