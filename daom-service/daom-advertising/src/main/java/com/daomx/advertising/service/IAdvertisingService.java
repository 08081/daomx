package com.daomx.advertising.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.daomx.advertising.vo.AdvertisingVo;
import com.daomx.entity.Advertising;

import java.util.List;

/**
 * <p>
 * 广告表 服务类
 * </p>
 *
 * @author xiaodao
 * @since 2019-08-16
 */
public interface IAdvertisingService extends IService<Advertising> {

	List<AdvertisingVo> queryAdvertisingList(Integer type, long currentPage, long pageSize);
}
