package com.daomx.advertising.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daomx.entity.Advertising;


/**
 * <p>
 * 广告表 Mapper 接口
 * </p>
 *
 * @author xiaodao
 * @since 2019-08-16
 */
public interface AdvertisingMapper extends BaseMapper<Advertising> {

}
