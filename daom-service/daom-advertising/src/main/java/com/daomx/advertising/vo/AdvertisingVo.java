package com.daomx.advertising.vo;

import lombok.Builder;
import lombok.Data;

/**
 * Created by xiaodao
 * date: 2019/8/16
 */
@Data
@Builder
public class AdvertisingVo {

	private String original;
}
