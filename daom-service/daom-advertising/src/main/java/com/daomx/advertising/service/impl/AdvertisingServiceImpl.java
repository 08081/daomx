package com.daomx.advertising.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daomx.advertising.vo.AdvertisingVo;
import com.daomx.entity.Advertising;
import com.daomx.advertising.mapper.AdvertisingMapper;
import com.daomx.advertising.service.IAdvertisingService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 广告表 服务实现类
 * </p>
 *
 * @author xiaodao
 * @since 2019-08-16
 */
@Service
public class AdvertisingServiceImpl extends ServiceImpl<AdvertisingMapper, Advertising> implements IAdvertisingService {

	@Override
	public List<AdvertisingVo> queryAdvertisingList(Integer type, long currentPage, long pageSize) {
		QueryWrapper<Advertising> queryWrapper  = new QueryWrapper<>();
		queryWrapper.eq("type",type);
		queryWrapper.orderByDesc("update_date");
		IPage<Advertising> objectIPage = new Page<>(currentPage, pageSize);
		IPage<Advertising> advertisingIPage =  baseMapper.selectPage(objectIPage, queryWrapper);
		List<AdvertisingVo> collect = advertisingIPage.getRecords().stream().map(
			o ->
				AdvertisingVo.builder().original(o.getUrl()).build()

			).collect(Collectors.toList());
		return collect;
	}
}
