package com.daomx.advertising.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.daomx.advertising.vo.AdvertisingVo;
import com.daomx.common.api.ResultMessage;
import com.daomx.entity.Advertising;
import com.daomx.advertising.service.IAdvertisingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/16
 */
@RestController
@RequestMapping("/advertising")
@CrossOrigin
public class AdvertisingController {

	@Autowired
	private IAdvertisingService advertisingServiceImpl;
	@RequestMapping(value = "/list")
	public ResultMessage<List<AdvertisingVo>> list(int type, long currentPage,long pageSize){
		List<AdvertisingVo> strings = advertisingServiceImpl.queryAdvertisingList(type, currentPage, pageSize);
		return ResultMessage.data(strings);
	}
}
