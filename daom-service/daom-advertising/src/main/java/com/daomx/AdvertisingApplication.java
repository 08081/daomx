package com.daomx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by xiaodao
 * date: 2019/8/16
 */
@SpringCloudApplication
@MapperScan(basePackages = {"com.daomx"})
@EnableDiscoveryClient
//@CrossOrigin
public class AdvertisingApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdvertisingApplication.class,args);
	}
}
