package com.daomx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by xiaodao
 * date: 2019/8/12
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.daomx"})
public class HouseResourcesApplication {
	public static void main(String[] args) {
		SpringApplication.run(HouseResourcesApplication.class,args);
	}
}
