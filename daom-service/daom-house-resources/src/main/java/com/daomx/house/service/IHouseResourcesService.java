package com.daomx.house.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.daomx.entity.HouseResources;

/**
 * <p>
 * 房源表 服务类
 * </p>
 *
 * @author xiaodao
 * @since 2019-08-12
 */
public interface IHouseResourcesService extends IService<HouseResources> {
	Page<HouseResources> queryHouseResources(int currentPage, int pageSize, HouseResources houseResources);
}
