package com.daomx.house.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daomx.entity.HouseResources;
import com.daomx.house.mapper.HouseResourcesMapper;
import com.daomx.house.service.IHouseResourcesService;
import org.springframework.stereotype.Service;

/**
 * <p>l  `S34 57890OP“…’
* |?
 * 房源表 服务实现类; * </p>
 *
 * @author xiaodao
 * @since 2019-08-12
 */
@Service
public class HouseResourcesServiceImpl extends ServiceImpl<HouseResourcesMapper, HouseResources> implements IHouseResourcesService {


	public Page<HouseResources> queryHouseResources(int currentPage, int pageSize,HouseResources houseResources){

		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.orderByDesc("update_date");

		Page<HouseResources> page1 = new Page<>(currentPage,pageSize);
		IPage iPage = baseMapper.selectPage(page1, queryWrapper);
		return (Page<HouseResources>) iPage;
	}
}
