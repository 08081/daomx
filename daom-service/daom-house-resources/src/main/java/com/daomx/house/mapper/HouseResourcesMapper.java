package com.daomx.house.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.daomx.entity.HouseResources;

/**
 * <p>
 * 房源表 Mapper 接口
 * </p>
 *
 * @author xiaodao
 * @since 2019-08-12
 */
public interface HouseResourcesMapper extends BaseMapper<HouseResources> {

}
