package com.daomx.house.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daomx.common.api.ResultMessage;
import com.daomx.entity.HouseResources;
import com.daomx.house.service.IHouseResourcesService;
import org.apache.logging.log4j.message.ReusableMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.POST;
import java.util.Date;
import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/12
 */
@RestController
@RequestMapping("/house")
@RefreshScope
@CrossOrigin
public class HouseController {

	private static final Logger log =  LoggerFactory.getLogger(HouseController.class);
	@Autowired
	private IHouseResourcesService houseResourcesServiceImpl;

	@Value(value = "${user.name}")
	public String name;

	@GetMapping("/name")
	public String getName(){
		return name;
	}

	@RequestMapping("/list")
	public ResultMessage<IPage<HouseResources>> list(int currentPage, int pageSize , HouseResources houseResources){
		Page<HouseResources> houseResourcesPage = houseResourcesServiceImpl.queryHouseResources(currentPage, pageSize, houseResources);
		System.out.println("222");
		log.info("查询");
		return  ResultMessage.data(houseResourcesPage);
	}

	@PostMapping("/save")
	public ResultMessage<HouseResources> save(@RequestBody HouseResources houseResources){
		houseResourcesServiceImpl.save(houseResources);
		return ResultMessage.status(true);
	}

	/*@GetMapping("/get/{id}")
	public ResultMessage<HouseResources> get(@PathVariable("id") String id){
		HouseResources houseResources = houseResourcesServiceImpl.getById(id);
		return ResultMessage.data(houseResources);
	}*/
	@PostMapping("/get")
	public ResultMessage<HouseResources> get(@RequestBody String id){
		HouseResources houseResources = houseResourcesServiceImpl.getById(id);
		return ResultMessage.data(houseResources);
	}

	@PutMapping("/update")
	public ResultMessage<HouseResources> update(@RequestBody HouseResources houseResources){
		houseResources.setUpdateDate(new Date());
		boolean b = houseResourcesServiceImpl.updateById(houseResources);
		return ResultMessage.status(b);
	}

}
