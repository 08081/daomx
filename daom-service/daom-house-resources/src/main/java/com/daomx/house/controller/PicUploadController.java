package com.daomx.house.controller;

import com.daomx.entity.PicUploadResult;
import com.daomx.house.service.impl.PicUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by xiaodao
 * date: 2019/8/14
 */
@RequestMapping("/pic/")
@RestController
public class PicUploadController {


	@Autowired
	private PicUploadService picUploadService;

	@PostMapping(value = "upload")
	public PicUploadResult upload(@RequestParam("file")MultipartFile multipartFile){
		return picUploadService.upload(multipartFile);
	}
}
