package com.daomx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.daomx.common.persitence.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by xiaodao
 * date: 2019/8/16
 * 系统广告
 */
@Data
@Accessors(chain = true)
@TableName("daom_advertising")
public class Advertising extends BaseEntity<Advertising> {

	/**
	 * 广告类型
	 */
	private Integer type;

	/**
	 * 描述
	 */
	private String title;
	/**
	 * 图片url
	 */
	private String url;
}

