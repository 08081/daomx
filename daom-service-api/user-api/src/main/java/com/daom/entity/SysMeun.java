package com.daom.entity;

import com.daomx.common.persitence.BaseEntity;
import lombok.*;

/**
 * Created by xiaodao
 * date: 2019/8/19
 */
@Builder
@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class SysMeun extends BaseEntity<SysMeun> {

	private String code;
}
