package com.daom.entity;

import com.daomx.common.persitence.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * Created by xiaodao
 * date: 2019/8/19
 */
@Builder
@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class SysUser extends BaseEntity<SysUser> {

	private String name;
	private String password;
	/**
	 * 用户类型
	 */
	private String type;
	private String companyId;
	/**
	 * 头像
	 */
	private String pic;

	private List<SysMeun> permissions;
}
