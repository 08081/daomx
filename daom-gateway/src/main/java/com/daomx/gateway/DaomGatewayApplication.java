package com.daomx.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by xiaodao
 * date: 2019/8/12
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DaomGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(DaomGatewayApplication.class,args);
	}
}
