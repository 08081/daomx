package com.daomx.common.persitence;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by xiaodao
 * date: 2019/8/12
 */
@Data
public class BaseEntity<T> implements Serializable {
	@TableId(value = "id")
	private String Id;
	private String createUser;
	private Date createDate;
	private String updateUser;
	private Date updateDate;
	private Integer isDeleted;
}
