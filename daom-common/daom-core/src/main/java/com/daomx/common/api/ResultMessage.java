package com.daomx.common.api;

import com.daomx.common.constant.DaomConstant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Optional;

/**
 * Created by xiaodao
 * date: 2019/8/13
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ResultMessage<T> implements Serializable {
	private int code;
	private boolean success;
	private T data;
	private String msg;
	private ResultMessage(IResultCode resultCode) {
		this(resultCode, null, resultCode.getMessage());
	}

	private ResultMessage(IResultCode resultCode, String msg) {
		this(resultCode, null, msg);
	}

	private ResultMessage(IResultCode resultCode, T data) {
		this(resultCode, data, resultCode.getMessage());
	}

	private ResultMessage(IResultCode resultCode, T data, String msg) {
		this(resultCode.getCode(), data, msg);
	}

	private ResultMessage(int code, T data, String msg) {
		this.code = code;
		this.data = data;
		this.msg = msg;
		this.success = ResultCode.SUCCESS.code == code;
	}

	/**
	 * 判断返回是否为成功
	 *
	 * @param result Result
	 * @return 是否成功
	 */
	public static boolean isSuccess(@Nullable ResultMessage<?> result) {
		return Optional.ofNullable(result)
			.map(x -> ObjectUtils.nullSafeEquals(ResultCode.SUCCESS.code,x.code))
			.orElse(Boolean.FALSE);
	}

	/**
	 * 判断返回是否为成功
	 *
	 * @param result Result
	 * @return 是否成功
	 */
	public static boolean isNotSuccess(@Nullable ResultMessage<?> result) {
		return !ResultMessage.isSuccess(result);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param data 数据
	 * @param <T>  T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> data(T data) {
		return data(data, DaomConstant.DEFAULT_SUCCESS_MESSAGE);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param data 数据
	 * @param msg  消息
	 * @param <T>  T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> data(T data, String msg) {
		return data(HttpServletResponse.SC_OK, data, msg);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param code 状态码
	 * @param data 数据
	 * @param msg  消息
	 * @param <T>  T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> data(int code, T data, String msg) {
		return new ResultMessage<>(code, data, data == null ? DaomConstant.DEFAULT_NULL_MESSAGE : msg);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param msg 消息
	 * @param <T> T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> success(String msg) {
		return new ResultMessage<>(ResultCode.SUCCESS, msg);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param resultCode 业务代码
	 * @param <T>        T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> success(IResultCode resultCode) {
		return new ResultMessage<>(resultCode);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param resultCode 业务代码
	 * @param msg        消息
	 * @param <T>        T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> success(IResultCode resultCode, String msg) {
		return new ResultMessage<>(resultCode, msg);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param msg 消息
	 * @param <T> T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> fail(String msg) {
		return new ResultMessage<>(ResultCode.FAILURE, msg);
	}


	/**
	 * 返回ResultMessage
	 *
	 * @param code 状态码
	 * @param msg  消息
	 * @param <T>  T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> fail(int code, String msg) {
		return new ResultMessage<>(code, null, msg);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param resultCode 业务代码
	 * @param <T>        T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> fail(IResultCode resultCode) {
		return new ResultMessage<>(resultCode);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param resultCode 业务代码
	 * @param msg        消息
	 * @param <T>        T 泛型标记
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> fail(IResultCode resultCode, String msg) {
		return new ResultMessage<>(resultCode, msg);
	}

	/**
	 * 返回ResultMessage
	 *
	 * @param flag 成功状态
	 * @return ResultMessage
	 */
	public static <T> ResultMessage<T> status(boolean flag) {
		return flag ? success(DaomConstant.DEFAULT_SUCCESS_MESSAGE) : fail(DaomConstant.DEFAULT_FAILURE_MESSAGE);
	}

}
