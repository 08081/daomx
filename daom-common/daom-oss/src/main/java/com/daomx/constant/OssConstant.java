package com.daomx.constant;

/**
 * Created by xiaodao
 * date: 2019/8/14
 * 上传通用信息
 */
public interface OssConstant {
	// 允许上传的格式
	public static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg",
		".jpeg", ".gif", ".png"};
}
