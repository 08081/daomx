package com.daomx.utils;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * Created by xiaodao
 * date: 2019/8/14
 * 文件工具类
 */
public class FileUtil {
	/**
	 * 生成路径 /yyyy/mm/dd/xxxx.jpg
	 * @param sourceFileName
	 * @return
	 */
	public static String getFilePath(String sourceFileName) {
		DateTime dateTime = new DateTime();
		return "images/" + dateTime.toString("yyyy")
			+ "/" + dateTime.toString("MM") + "/"
			+ dateTime.toString("dd") + "/" + System.currentTimeMillis() +
			RandomUtils.nextInt(100, 9999) + "." +
			StringUtils.substringAfterLast(sourceFileName, ".");
	}
}
