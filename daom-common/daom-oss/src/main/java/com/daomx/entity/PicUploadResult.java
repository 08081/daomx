package com.daomx.entity;

import lombok.Data;

/**
 * Created by xiaodao
 * ant.design 上传图片的返回格式
 * date: 2019/8/14
 */
@Data
public class PicUploadResult {
	/**
	 * 文件唯一标识，建议设置为负数，防止和内部产生的 id 冲突  'uid',
	 */
	private String uid;
	/**
	 * 文件名   'xx.png'
	 */
	private String name;
	/**
	 * 状态有：uploading done error removed  'done',
	 */

	private String status;
	/**
	 * 服务端响应内容'{"status": "success"}'
	 */
	private String response;
}
