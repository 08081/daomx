package com.daomx.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by xiaodao
 * date: 2019/8/14
 */
@Configuration
@PropertySource("classpath:aliyun.properties")
@ConfigurationProperties(prefix = "aliyun")
@Data
public class OSSconfig {

	private String 	endpoint;
	private String 	accessKeyId;
	private String 	accessKeySecret;
	private String  bucketName;
	private String  urlPrifix;

	@Bean
	public OSS ossClient(){
		OSS ossClient =new  OSSClientBuilder().build(endpoint,accessKeyId,accessKeySecret);
		return ossClient;
	}
}
